\documentclass{article}
\usepackage{amsmath}
\usepackage{array}
\usepackage[margin=4cm]{geometry}
\usepackage{booktabs}
\usepackage{authblk}
\usepackage[square,numbers]{natbib}
\usepackage[hidelinks]{hyperref}
\setlength{\parindent}{0em}
\setlength{\parskip}{1em}
\bibliographystyle{abbrvnat}

\title{Internship Report\\Time Series Data Cleaning Techniques}
\author{Vitaliy Pozdnyakov}
\date{}

\begin{document}

\begin{titlepage}
    \begin{center}
        
        \vspace*{1cm}
        
        \large
        \textbf{Higher School of Economics -- National Research University}\\
        Faculty of Computer Science\\
        
        \vspace{1cm}
        
        \Large
        \textbf{Internship Report}

        \vspace{1cm}

        \large
        \textbf{Student:} Vitaliy Pozdnyakov\\

        \vspace{1cm}

        \textbf{Place of Internship:} Laboratory of Methods for BigData  Analysis  at  HSE  University\\
        
        \vspace{1cm}
        
        \textbf{Assignment:} Time Series Data Cleaning Techniques\\
             
        \vspace{1cm}
 
        \textbf{Internship Supervisor}: Mikhail Hushchyn, research scientist

 
        \vfill
             
        Moscow, 2020
             
    \end{center}
 \end{titlepage}
 
\tableofcontents

\section{Introduction} \label{introduction}

This report was written as part of my internship at the Laboratory of Methods for Big Data Analysis at HSE University in 2020. During my internship, I was assigned the following tasks:
\begin{itemize}
    \item To review and develop of time series cleaning algorithms based on synthetic and real data
    \item To conduct computational experiments and provision of results
\end{itemize}

In real life, quite often there are errors in the time series that occur due to equipment failures, temporary disconnection of sensors, etc. Such errors can greatly affect the quality of predictive machine learning models that use historical data. To avoid this, various data cleaning techniques are used, which are applied to time series and then pass cleaned data to predictive models for analysis. 

Strictly speaking, given time series $X = \{x_i\}_{i=1}^n$ with anomaly series $\varepsilon = \{\varepsilon_i\}_{i=1}^n$. In common case $\varepsilon_i =0 $, but in some random time moments $k \in [1, n]$, $\varepsilon_k \sim \mathcal{N}(\mu, \sigma^2)$. The cleaning process $\mathcal A$ with hyperparameters $\theta$ should reconstruct the original time series $\tilde X = \mathcal A(X + \varepsilon, \theta)$ as precise to $X$ as possible. We want to minimize the error of reconstruction for some metric $\rho$ with respect to cleaning process $\mathcal A$ and hyperparameters $\theta$, that is
$$\rho(X, \tilde X) \to \min_{\mathcal A, \theta}$$
The time series cleaning process can be divided into three stages:

\begin{enumerate}
    \item Preparing data, that is converting the original time series into a form that facilitates the search for anomalies. For example, it may be normalization of time series values.
    \item Anomaliy detection in time series, that is search for observations that stand out strongly from the mass of other observations. For example, in the case of an air temperature measurement, this may be an observation with an abnormally high temperature against the background of the values before and after this observation.
    \item The reconstruction of the true values after removal of anomalies. For example, it can be linear interpolation based on neighboring observations.
\end{enumerate}

In this report, I consider several algorithms for each stage of time series cleaning process and compare the quality of their work. Also I introduce some approaches to implementation of well known algorithms with respect to this specific problem.

This report is organized as follows: the next section (\ref{algorithms}) consists of brief algorithms description, the next one (\ref{datasets}) contains dataset generation procedure and real dataset description. In the section (\ref{metrics}), I introduce the quality metric Relative Error (RE) that is used for comparison. Finally, result of experiments will be described in section (\ref{results}). The last section (\ref{conclusion}) is dedicated to summarizing results and directions of further works.

\section{Algorithms} \label{algorithms}
\subsection*{Data preprocessing}
\textbf{Exponential smoothing (ES).} The algorithm \cite{HOLT20045} is used for smoothing time series. Let the original time series be $X = \{x_i\}_{i=0}^n$, then the smoothed time series $S=\{s_i\}_{i=1}^n$ will be
\begin{equation*}
    \begin{cases}
        s_0 = x_0\\
        s_t = \alpha x_t + (1 - \alpha)s_t, t>0
    \end{cases}
\end{equation*}
where $\alpha \in (0, 1)$ is the smoothing factor. Using such smoothing I calculate the differences between smoothed and original time series
$$D = X - S$$
where $D$ is used as input time series data in an anomalies detection algorithm.

\textbf{Random forest (RF).} The random forest \cite{hastie01statisticallearning} is a machine learning model that is advanced version of the decision tree. Random forest combines several random trees that trained independently for different part of the training set. Every tree makes prediction independently and then the algorithm choose an average prediction among all trees. In the preprocessing task, Random forest makes a prediction in each time moment using $m$ previous observations
$$s_i = \text{RF}(x_{i-1}, x_{i-2}, \dots, x_{i-m})$$
and then the differences between predicted and original time series are calculated as was described above. Since I cannot apply RF to the $m$ first observations, I apply ES instead of RF there.

\textbf{Neighbors' difference} is a simple normed differences between neighboring observation of the form
\begin{equation*}
\begin{cases}
s_0 = 0\\
s_t = \frac{x_t - x_{t-1}}{\Vert \mathbf x \Vert}
\end{cases}
\end{equation*}
where $\mathbf x = (x_0, \dots, x_n)^T$.

\textbf{Linear regression} is a machine learning model that minimize squared distances between the observations and fitted multidimensional linear function. An optimization problem can be expressed as
$$(y - X\beta)^T(y-X\beta) \to \min_\beta$$
where $y$ is a vector of predictions, $X$ is an autoregression matrix of the form 
\begin{equation*}
X = 
\begin{pmatrix}
x_{11} & x_{12} & \dots & x_{1m} \\
x_{21} & x_{22} & \dots & x_{2m} \\
\dots \\
x_{n1} & x_{n2} & \dots & x_{nm} \\
\end{pmatrix}    
\end{equation*}
where the row $x_{11}, x_{12}, \dots, x_{1m}$ contains $m$ previous observations for $y_1$ and so on. In this case, Linear regression is used as RF — makes a prediction based on $m$ previous observations and then the differences between the original and predicted time series are calculated and used for anomaly detection in the next stage.

\textbf{Ridge regression} is also a linear model \cite{doi:10.1080/00401706.1970.10488634} that is fitted with respect to least squared distances and an additional regularization for some $c>0$
$$(y - X\beta)^T(y-X\beta) + \lambda(\beta^T\beta - c) \to \min_\beta$$
After fitting $\beta$, the model is used to predict the observations and then I calculate differences as was described above. The differences between the original and predicted time series are calculated and used for anomaly detection in the next stage

\textbf{Gradient boosting} is a machine learning technique \cite{10.5555/3009657.3009730} that combines a set of weak models, for example decision trees, into single strong model. There are $M$ stages where each stage $m = 1, \dots, M$ has a week model
$$F_{m+1} (X) = F_{m}(X) + h_m(X)= y$$
where $h_m(X)$ is a new model, that is the residuals $h_m(X) = y - F_m(X)$ will fit separetely from $F_m(X)$. The differences between the original and predicted time series are calculated and used for anomaly detection in the next stage

\textbf{Seasonal-Trend Decomposition Procedure Based in Loess (STL)} is iterative procedure \cite{hyndman2018forecasting} for decomposing time series data into three components: trend $T$, seasonal $S$, and remainder $R$ that is
$$X_t = T_t + S_t + R_v$$
The method is based on locally weighted scatterplot smoothing (LOESS) also known as moving regression. The time series is considered as data points (scatterplot) $(x_i, y_i)$, where $x_i$ is a time moment (independent variable) and $y_i$ is a value (dependent variable). The smoothed value of a dependent variable is obtained from a polynomial that fitted using weighted least squares of subset of nearest neighbors. A remainder component is used to anomaly detection.

\textbf{Additive decomposition based on mooving average} is a simple time series decomposition procedure \cite{hyndman2018forecasting} that produces trend, seasonal, and remainder components. The first step is calculation a trend component using a mooving average. In the second step, a seasonal component is obtained from the difference between initial dataset and trend component by seasonal average. The third step, residuals is calculated by subtracting the estimated seasonal and trend components. Then the residuals is used for anomaly detection.

\textbf{Moving average} is used for calculation smoothing dataset. Then the smoothing values is subtracted from the initial dataset and the result is used for anomaly detection.

\textbf{Normalization} is a typical operation for time series data to adjust all other algorithms to the same range of possible values.

\subsection*{Anomaly detection}

These algorithms is applied to the result of data preprocessing stage.

\textbf{Elbow threshold filtering} is introduced in this work. The method is based on the simple threshold cutoff and the elbow method. Let us define a threshold as a lower and upper bounds $L, U$. All beyond observations is considered as outliers
$$\text{Outliers} = \{x_i: x_i \notin [L, U]\}$$
The threshold is iteratively calculated as follows:
\begin{enumerate}
    \item Set a lower and upper bounds equal to average value of time series
        $$L_0 = U_0 = \frac{1}{N}\sum_{i=1}^N x_i$$
        Obviously, the number of outliers $K_0 \approx N$ with this threshold.
    \item Increment some fixed $\varepsilon$ to the threshold
    $$L_i = L_{i-1} - \varepsilon$$
    $$U_i = U_{i-1} + \varepsilon$$
    \item Calculate the number of outliers by given threshold
    $$K_i = \text{card}(\{x_i: x_i \notin [L_i, U_i]\})$$
    \item Repeat 2-3 while $K_i \geq 0$
\end{enumerate}
Note that $\{K_i\}$ is a decreasing sequence. We want to find a value from which the sequence does not decrease rapidly. Let $\Delta K_i = K_i - K_{i+1}$ and select $\Delta K' = \{\Delta K_i: \Delta K_i < \alpha \cdot N\}$ where $\alpha$ is some small constant. Then define the best thershold by an index of the first member of $\Delta K'$. For example, if $\Delta K' = \{ \Delta K_5, \Delta K_6, \dots\}$ then the best threshold is $L_5, U_5$.

The algorithm uses hyperparameters $\varepsilon$ and $\alpha$.

\textbf{DBSCAN} is a distance based clustering algorithm \cite{10.1145/3068335} that uses a hyperparater $\varepsilon$ and integer number $n$. Observations are considered as data point cloud. In the first step, the algorithm takes a random data point and calculates the number of neighbors inside a sphere of radius $\varepsilon$ with a center in the taken point. If the number is more or equal to $n$, then these points are labeled as the first cluster. Otherwise, these points are labeled as noise. In the next steps, the alogorithm repeats this procedure with members of the cluster trying to find new members. If there are no points nearby other than noise, the next random data point is taken and the procedure is repeated in the same way, creating new clusters. 

In this work I introduce an approach to find the hyperparameter $\varepsilon$
\begin{enumerate}
    \item Choose an interval of $[\varepsilon_{\min}, \varepsilon_{\max}]$ where $\varepsilon_{\min}$ is equals to a maximal $\varepsilon$ such that DBSCAN gives only noise. Also, $\varepsilon_{\max}$ is equals to a minimal $\varepsilon$ such that DBSCAN gives a single cluster.
    \item Iteratively run DBSCAN scanning values in the interval with some step and calculate the size of the largest cluster.
    \item Choose $\varepsilon$ that gives the largest growth of the largest cluster.
\end{enumerate}

In this task, I consider all points outside the largest cluster as anomalies.

\textbf{Spectral clustering} \cite{cite-key} is uses the number clusters as a single hyperparameter. This method is based on eigenvectors of the Laplacian matrix of the complete graph with distance based weights of edges. The complete graph is obtained from the data using distance between points. 

In this task, I fix the number of clusters as 2 and transform input data as follows 
$$x'_i = \begin{cases}
    x_i, \text{if }x_i \geq \bar X\\
    - x_i + \bar X, \text{otherwise}
\end{cases}$$

where $\bar X$ is an average of $X$. I consider a cluster that nearby an average as residual series and another cluster as noise.

\textbf{Local outlier factor} \cite{10.1145/335191.335388} is based on local density that is estimated by $k$ nearest neighbor distance. The algorithm compares local density of the points with local density of their neighbors. The points with noticably lower local density than their neighbors are considered as outliers. 

\textbf{Elliptic envelope} \cite{article} a method of anomaly detection that is based on robust estimation of multivaraitive Gaussian distribution. All points that is located far away the mode of distribution are considered as anomalies.

\textbf{Isolation forest} \cite{4781136} generates random slittings of values of data points between minimum and maximum values. Since the partition of the data points can be represented as a tree, the path length from the root to any value can be represented as distance. Points with small distance are considered as anomalies.

\textbf{One-class SVM with non-linear kernel} \cite{10.1162/089976601750264965} constructs a frontier-delimited subspace using Suppor Vector Machine (SVM) model with Radial Basis Function (RBF) kernel. All points outside the subspace are considered as anomalies.

\textbf{Prophet} is a Python package\footnote{\url{https://facebook.github.io/prophet/}} that is used for time series forecasting. The predict can be represented as:
$$y(t) = g(t) + s(t) + h(t) + \varepsilon (t)$$
where $g(t)$ — a trend, $s(t)$ — seasonal series, $h(t)$ — a holiday effect (special events) and $\varepsilon(t)$ — a stochastic process. The key idea of this framework is define priors on the parameters of terms $g(t)$, $s(t)$, and $h(t)$ and then sample the posterior distribution using Markov Chain Monte Carlo methods to find the maximum likelihood of $y(t)$.

From this point of view, I use the 80\% confidence interval of a prediction of Prophet package and consider all points outside the interval as anomalies.

\textbf{Luminol} is a Python package\footnote{\url{https://github.com/linkedin/luminol}} that is used for anomaly detection. There are several algorithms, but I use the default detector. It consists of Exponential Moving Average, that uses a data point's deviation from the exponential moving average of a lagging window
to determine its anomaly score.

\subsection*{Time series reconstruction}

These algorithms is applied to the result of anomaly detection stage.

\textbf{Linear interpolation} is linear function that defined as
$$x_2 = f(x_1, x_3) = \frac{x_1 + x_3}{2}$$
where $x_2$ is an anomaly and $x_1, x_3$ are regular observations. If there is a sequence of anomalies then the interpalation is applied a few times. For example, $x_1, x_5$ are regular and $x_2, x_3, x_4$ are anomalies. Then the first step will be
$$x_3 = \frac{x_1 + x_5}{2}$$ 
and then
$$x_2 = \frac{x_1 + x_3}{2}$$ 
$$x_4 = \frac{x_3 + x_5}{2}$$

\textbf{Spline intrepolation} is a method of interpolation that represents an initial set of points as a piecewise polynomials. Spline intrepolation consists of two steps:
\begin{enumerate}
    \item Calculation of a spline representation of the curve
    \item Evaluation of a spline at the desired points
\end{enumerate}
In this task, I use polynomials of degree 3.

\section{Datasets Description} \label{datasets}

To test the performance of the algorithms I consider several datasets. Among them there are synthetic and real time series. Anomalous observations were added to these time series, so two versions of each dataset were obtained: with and without anomalies. Anomalies were added using the following algorithm:

\begin{itemize}
    \item The starting positions of anomalies are randomly selected. The number of starting positions is 5\% of all observations.
    \item For each starting position, the duration of the anomaly is randomly selected, from 1 to 5 observations.
    \item Noise from the normal distribution with parameters $\mu=0$ and $\sigma = 5\hat\sigma$ is added to each selected observation, where $\hat\sigma$ is the standard deviation of the given time series.
\end{itemize}
A comprehensive description of all datasets is provided below:

\begin{center}
\begin{tabular}{| m{7em} | m{5em} | m{5em} | m{15em} |}
\hline
\textbf{Dataset name} & \textbf{Type} & \textbf{Number of observations} & \textbf{Description} \\
\hline
\hline
Constant2K & Synthetic & 2000 & \small Constant function $f(x) = 1$ \\
\hline
Lin2K & Synthetic & 2000 & \small Linear function $f(x) = 0.01x + 1$ \\
\hline
Sin2K & Synthetic & 2000 & \small Sine way $f(x) = \sin(0.1x) + 1$ \\
\hline
SinLin2K & Synthetic & 2000 & \small Sine way with linear increments $f(x) = 0.01x + \sin(0.1x) + 1$ \\
\hline
MixSinLin2K & Synthetic & 2000 & \small Sine ways mixture with linear increments $f(x) = 0.01x + \sin(0.1x) + \sin(0.012x) + 1$ \\
\hline
AirTemp9K & Real & 9358 & \small Air temperature \\
\hline
RelHum9K & Real & 9358 & \small Relative Humidity (\%) \\
\hline
AbsHum9K & Real & 9358 & \small Absolute Humidity \\
\hline
HourResp9K & Real & 9358 & \small Hourly averaged sensor response (nominally 
carbon monoxide targeted) \\
\hline
\end{tabular}
\end{center}

All real datasets was obtained from the UCI repository Air Quality Data Set\footnote{\url{https://archive.ics.uci.edu/ml/datasets/Air+Quality}}.

\section{Evaluation Metric} \label{metrics}

Since there are several datasets involved in the experiment, I introduced an aggregated metric for quality assessment. Relative Error (RE) is a metric based on the RMSE:

$$\text{RE} = \frac{1}{n}\sum_{i=1}^n \frac{\text{RMSE}\left[A(X_i)\right]}{\text{RMSE}\left[\text{Baseline}(X_i)\right]},$$
where $X_i$ is an $i$-th dataset, $A$ is a cleaning algorithm, Baseline is the trivial algorithm that do not touch the data and skip all anomalies.

\section{Experimental Results} \label{results}

All experiments is conducted with linear interpolation. The datasets and source code can be found in the GitLab repository\footnote{\url{https://gitlab.com/lambda-hse/energy-forecast/summer-anomaly}}.

\begin{center}
\begin{tabular}{llrr}
\toprule
Preprocessing & Detection & RE &  Deviation\\
\midrule
Random forest & DBSCAN &  0.103047 &   0.024855 \\
Exponention smoothing & Elbow  threshold  filtering &  0.111782 &   0.034908 \\
Neighbors' difference & Spectral clustering &  0.126148 &   0.048330 \\
Exponention smoothing & DBSCAN &  0.126416 &   0.020852 \\
Neighbors' difference & DBSCAN &  0.133170 &   0.087380 \\
Linear regression & DBSCAN &  0.134778 &   0.097697 \\
Ridge regression & DBSCAN &  0.135168 &   0.097999 \\
Gradient boosting & DBSCAN &  0.153596 &   0.088989 \\
Neighbors' difference & Elbow  threshold  filtering &  0.153825 &   0.090630 \\
STL decomposition & DBSCAN &  0.154641 &   0.023832 \\
\hline
Exponential smoothing & Local outlier factor &  0.166207 &   0.032691 \\
& Elliptic envelope &  0.170088 &   0.034635 \\
& Isolation forest &  0.171748 &   0.031803 \\
& SVM &  0.172946 &   0.033881 \\
\hline
Normalization & Prophet &  0.188119 &   0.102221 \\
Additive decomposition & DBSCAN &  0.238772 &   0.192679 \\
Normalization & Luminol &  0.480883 &   0.165741 \\
\bottomrule
\end{tabular}
\end{center}
\section{Conclusion} \label{conclusion}

During my internship, I got acquainted with various approaches to solving time series cleaning problems. I have reviewed several algorithms and design two own algorithms: elbow threshold filtering and selection $\varepsilon$ in DBSCAN. Also, I have conducted plenty numerical experiments and seen that a model with the lowest error is
\begin{itemize}
    \item Data preprocessing: Random Forest
    \item Anomaly detection: DBSCAN
    \item Time series reconstruction: Linear intreplation
\end{itemize}

Not all possible combinations of preprocessing, detection, reconstruction stages and all their hyperparameters were explored. So, the further researches can be resumed to the direction of greedy or stochastic search trying to find a better combination. In addition, researches would be dedicated to design rules for selection the best model with respect to a dataset.

\bibliography{main}

\end{document}